
export var recognition;

try {
    var SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
    recognition = new SpeechRecognition();
  }
  catch(e) {
    console.error(e);
    $('.no-browser-support').show();
    $('.app').hide();
  }

  export var listening;

  recognition.onstart = function() { 
    listening = true       
  }

  recognition.onend = function() { 
    listening = false       
  }





  