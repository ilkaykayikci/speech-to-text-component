# speech-to-text-component

A web component which does speech recognition.

# How to use

Use either yarn or npm to install.

yarn add https://gitlab.com/ilkaykayikci/speech-to-text-component.git

or

npm install https://gitlab.com/ilkaykayikci/speech-to-text-component.git --save
