import {
  LitElement,
  html,
  css
} from 'lit-element';
import {
  listening,
  recognition
} from './util';

/**
 * An example element.
 *
 * @slot - This element has a slot
 * @csspart button - The button
 */
export class SpeechToTextElement extends LitElement {
  static get styles() {
    return css `
      :host {
        display: block;
        border: solid 1px gray;
        padding: 16px;
        max-width: 800px;
      }
    `;
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();

  }

  render() {
    return html ` 
    <textarea value=${this.text} id="txtarea"></textarea>
    <button @click=${this._onStart} part="button">
      Aufnahme starten!
    </button>
    <button @click=${this._onStop} part="button">
      Aufnahme beenden!
    </button>    
    <p>Aufnahme starten klicken und lossprechen.</p>    
  `;
  }

  _onStart() {

    var noteTextarea = this.shadowRoot.getElementById('txtarea');
    var noteContent = '';


    if (!listening) {
      recognition.start()


      recognition.continuous = true;

      recognition.onresult = function (event) {

        var current = event.resultIndex;
        var transcript = event.results[current][0].transcript;
        var mobileRepeatBug = (current == 1 && transcript == event.results[0][0].transcript);

        if (!mobileRepeatBug) {
          noteContent += transcript;
          noteTextarea.value = noteContent;
        }

      }
    }
  }

  _onStop() {
    recognition.stop()
  }
}

window.customElements.define('speechtotext-element', SpeechToTextElement);